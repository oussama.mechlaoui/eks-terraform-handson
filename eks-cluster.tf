# EKS terraform module

variable "account_id" {
  type            = string
  description     = "Enter your account id"
}

variable "admin_user_name" {
  type = string
  description = "Enter your aws user"
  
}

variable "keypair" {
  type = string
  description = "Enter your keypair used for SSHing worker nodes"
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = local.cluster_name
  cluster_version = "1.21"
  subnets         = module.vpc.private_subnets

  tags = {
    Environment = "training"
  }

  vpc_id = module.vpc.vpc_id
  map_users = [
      {
        userarn = "arn:aws:iam::${var.account_id}:user/${var.admin_user_name}"
        username = "admin-user"
        groups = ["system:masters"]
      },
    ]
  map_roles = [
      {
        rolearn = "arn:aws:iam::${var.account_id}:role/eks-node-group-terraform"
        username = "system:node:{{EC2PrivateDNSName}}"
        groups = ["system:bootstrappers","system:nodes"]
      },
    ]

}

#VPC CNI

variable "vpc_cni_addon_version" {
  type        = string
  default     = "v1.9.0-eksbuild.1"
}

resource "aws_eks_addon" "vpc_cni" {
  cluster_name      = local.cluster_name
  addon_name        = "vpc-cni"
  addon_version     = var.vpc_cni_addon_version
  resolve_conflicts = "OVERWRITE"
  depends_on = [module.eks]
}

# NodeGroup IAM Role

resource "aws_iam_role" "ng_iam_role" {
  name = "eks-node-group-terraform"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "ng_iam_role-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.ng_iam_role.name
}

resource "aws_iam_role_policy_attachment" "ng_iam_role-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.ng_iam_role.name
}

resource "aws_iam_role_policy_attachment" "ng_iam_role-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.ng_iam_role.name
}

# NodeGroup worker nodegroup

resource "aws_eks_node_group" "terraform-ng" {
  cluster_name    = local.cluster_name
  node_group_name = "managed-nodgroup"
  node_role_arn   = aws_iam_role.ng_iam_role.arn
  subnet_ids      = [module.vpc.private_subnets[0],module.vpc.private_subnets[1],module.vpc.private_subnets[2]]
  
  
  remote_access {
    ec2_ssh_key = var.keypair
  }

  scaling_config {
    desired_size = 3
    max_size     = 3
    min_size     = 3
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_eks_addon.vpc_cni,
    aws_iam_role_policy_attachment.ng_iam_role-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.ng_iam_role-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.ng_iam_role-AmazonEC2ContainerRegistryReadOnly,
  ]
}

resource "aws_cloud9_environment_ec2" "example" {
  instance_type = "m5.large"
  name          = "eks-env"
  subnet_id = module.vpc.public_subnets[0]
  owner_arn = "arn:aws:iam::${var.account_id}:user/${var.admin_user_name}"
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}
